package ua.com.anya.TodoMVCTest_v4;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.com.anya.TodoMVCTest_v4.categories.Smoke;
import ua.com.anya.TodoMVCTest_v4.features.TodosE2ETest;
import ua.com.anya.TodoMVCTest_v4.features.TodosOperationsAtAllFilterTest;

@RunWith(Categories.class)
@Suite.SuiteClasses({TodosE2ETest.class, TodosOperationsAtAllFilterTest.class})
@Categories.IncludeCategory(Smoke.class)
public class SmokeSuiteTest {
}
