package ua.com.anya.TodoMVCTest_v4;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.com.anya.TodoMVCTest_v4.categories.Buggy;
import ua.com.anya.TodoMVCTest_v4.features.TodosOperationsAtAllFilterTest;

@RunWith(Categories.class)
@Suite.SuiteClasses(TodosOperationsAtAllFilterTest.class)
@Categories.IncludeCategory(Buggy.class)
public class BuggySuiteTest {
}
